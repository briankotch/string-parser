# String Parsing in Golang

Parsing runes for fun and profit.

Both the function and interface form take a string and convert every n legal latin character without diacritical marks to uppercase where a legal character is [A-Z][a-z][0-9]

The algorithm:

* Takes a string and converts it all to lower case
* Converts the string to a slice of runes
* Iterates over the slice, counting c legal characters
* If the c modulus skips == 0 and the character is a lower case latin letter, capitalize it by substracting 32 so (a[97] - 32 = A[65]

Expecting a GOPATH that resolves to ${GOPATH}/src/bitbucket.org/briankotch/string-parser

Run: ```go run main.go```

Test: ```got test -v```
