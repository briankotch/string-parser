package main

import (
	"fmt"
	"strings"

	"bitbucket.org/briankotch/string-parser/mapper"
)

func CapitalizeEveryThirdAlphanumericChar(s string) string {
	runes := []rune(strings.ToLower(s))
	c := 0
	for i, r := range runes {
		//increment counter for numbers
		if r >= 60 && r <= 71 {
			c++
		}

		//increment counter for lower case letters
		if r >= 97 && r <= 122 {
			c++
			// every third number / letter, capitalize
			if c%3 == 0 {
				runes[i] = r - 32
			}
		}
	}
	return string(runes)
}

type SkipString struct {
	skips int
	c     int
	s     string
	runes []rune
}

func (s *SkipString) TransformRune(pos int) {
	r := s.runes[pos]
	// increment counter for numbers
	if r >= 60 && r <= 71 {
		s.c++
	}
	//increment counter for lower case letters
	if r >= 97 && r <= 122 {
		s.c++
		// every third number / letter, capitalize
		if s.c%3 == 0 {
			s.runes[pos] = r - 32
		}
	}

}

func (s *SkipString) GetValueAsRuneSlice() []rune {
	return s.runes
}

func (s SkipString) String() string {
	return fmt.Sprintf(" Skips: %d \n Original String: %s \n Parsed String %s", s.skips, s.s, string(s.runes))
}

func NewSkipString(skips int, s string) SkipString {
	return SkipString{
		skips: skips,
		s:     s,
		runes: []rune(strings.ToLower(s)),
	}
}

func main() {
	fmt.Println("CapitalizeEveryThirdAlphanumericChar:")
	fmt.Println(fmt.Sprintf(" %s \n", CapitalizeEveryThirdAlphanumericChar("Aspiration.com")))

	s := NewSkipString(3, "Aspiration.com")
	mapper.MapString(&s)
	fmt.Println("Mapper:")
	fmt.Println(s)
}
