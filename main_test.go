package main

import (
	"fmt"
	"testing"

	"bitbucket.org/briankotch/string-parser/mapper"
)

type TestCase struct {
	test, result string
}

var TEST_CASES = []TestCase{
	{"asPirAtiOn.cOm", "asPirAtiOn.cOm"},
	{"", ""},
	{"aaaaaaaaaaaaaa", "aaAaaAaaAaaAaa"},
	{"^@", "^@"},
}

func TestCapitalizeEveryThirdAlphanumericChar(t *testing.T) {
	for i, tc := range TEST_CASES {
		t.Logf("Case %d", i)
		s := CapitalizeEveryThirdAlphanumericChar(tc.test)
		if s != tc.result {
			t.Error(fmt.Sprintf("Expected '%s' from '%s'", tc.result, tc.test))
		}
	}
}

func TestMapper(t *testing.T) {
	for i, tc := range TEST_CASES {
		t.Logf("Case %d", i)
		s := NewSkipString(3, tc.test)
		mapper.MapString(&s)
		if string(s.runes) != tc.result {
			t.Error(fmt.Sprintf("Expected '%s' from '%s' got '%s", tc.result, tc.test, string(s.runes)))
		}
	}
}
